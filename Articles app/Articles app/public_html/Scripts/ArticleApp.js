
var ArticleApp = angular.module('myArticleList', ['ngStorage']);
ArticleApp.controller("myCtrl", function ($scope, $localStorage, $timeout) {

//Local storage to save article information
    $scope.$storage = $localStorage.$default({
        Articles: [
            {ArticleName: "Front end web development", ArticleInfo: "Front-end leaders help you to stay on top of relevant news and trends. They are in-the-know and they work on a specific topic.Twitter can be a great place to find people who are in-the-know.", imgSrc: "Images/UYxw-HcD_400x400.png"},
            {ArticleName: "what about angular js ?", ArticleInfo: "Angular 1.5 has been finally released and it’s more powerful than ever before! Many new features have been added and tons of fixes landed in the latest bigger release. If you’re following our articles, you know that we love to", imgSrc: "Images/images.png"}        
        ]
    });


    //Function of create values of html controls in object and push it in array  Articles
    // when one of html control with out value or Article name duplicated show error message 
    // 
    // and call it when click on add article btn
    $scope.imageUpload = function (event) {
        var files = event.target.files; //FileList object

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(file);
        }
    };

    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.image = e.target.result;
        });
    };

    $scope.addArticle = function () {
        $scope.errortext = "";
        $scope.IsValidArticle = true;
        $scope.ArticleObj = {
            ArticleName: $scope.addName,
            ArticleInfo: $scope.addInfo,
            imgSrc: $scope.image

        };

        if (!$scope.ArticleObj.ArticleName || !$scope.ArticleObj.ArticleInfo) {
            $scope.errortext = "Fill article information";
            return;
        }

        for (var i in $scope.$storage.Articles) {
            if ($scope.$storage.Articles[i].ArticleName === $scope.addName) {
                $scope.IsValidArticle = false;
            }
        }

        if ($scope.IsValidArticle == true) {
            $scope.$storage.Articles.push($scope.ArticleObj);
        } else {
            $scope.errortext = "";
            $scope.errortext = "The article is already exist ..";
        }
    };
    //function take index of article as parameter to delete article from list of articles
    $scope.deleteArticle = function (Artic) {
        $scope.$storage.Articles.splice(Artic, 1);

    };
});











